import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpErrorHandler, HandleError } from './others/http-error-handler.service';
import { Router } from '@angular/router';
import { Dominio } from '../models/dominio.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class DominioService {
  private handleError: HandleError;
  apiUrl: string;

  constructor(
    private http: HttpClient,
    private router: Router,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('DominioService');
    this.apiUrl = environment.apiUrl + 'Dominios';
  }

  getDominio(): Observable<Dominio[]> {
    return this.http.get<Dominio[]>(`${this.apiUrl}`)
      .pipe(
        catchError(this.handleError('getDominio', []))
      );
  }

  getDominioId(id: string): Observable<any[] | Dominio> {
    return this.http.get<Dominio>(`${this.apiUrl}/${id}`)
      .pipe(
        catchError(this.handleError('getDominioId', []))
      );
  }

  createDominio(dominio: Dominio): Observable<Dominio> {
    const body = JSON.stringify(dominio);
    return this.http.post<Dominio>(`${this.apiUrl}`, body, httpOptions)
      .pipe(
        catchError(this.handleError('createDominio', dominio))
      );
  }

  deleteDominio(id: string): Observable<{}> {
    return this.http.delete(`${this.apiUrl}/${id}`, httpOptions)
      .pipe(
        catchError(this.handleError('deleteDominio'))
      );
  }

  updateDominio(dominio: Dominio): Observable<Dominio> {
    return this.http.put<Dominio>(`${this.apiUrl}/${dominio.id}`, dominio, httpOptions)
      .pipe(
        catchError(this.handleError('updateDominio', dominio))
      );
  }
}
