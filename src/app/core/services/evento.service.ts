import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpErrorHandler, HandleError } from './others/http-error-handler.service';
import { Evento } from '../models/evento.model';
import { Router } from '@angular/router';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class EventoService {
  private handleError: HandleError;
  apiUrl: string;

  constructor(
    private http: HttpClient,
    private router: Router,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('EventoService');
    this.apiUrl = environment.apiUrl + 'Eventos';
  }

  getEvents(): Observable<Evento[]> {
    return this.http.get<Evento[]>(`${this.apiUrl}`)
      .pipe(
        catchError(this.handleError('getEvents', []))
      );
  }

  getEventsId(id: string): Observable<any[] | Evento> {
    return this.http.get<Evento>(`${this.apiUrl}/${id}`)
      .pipe(
        catchError(this.handleError('getEventsId', []))
      );
  }

  searchEventos(nombre: string, responsableID: string, estadosIds: number[], sedesIds: number[],
                fechaDesde: Date, fechaHasta: Date, inscritoID: string, asistenteID: string
                ): Observable<Evento[]> {

    let buscarUrl = `${this.apiUrl}/buscar?`;

    if (nombre !== undefined && nombre != null && nombre.trim() !== '') {
      buscarUrl = buscarUrl + '&nombre=' + nombre.trim();
    }

    if (responsableID !== undefined && responsableID != null && responsableID.trim() !== '') {
      buscarUrl = buscarUrl + '&responsableID=' + responsableID.trim();
    }

    if (estadosIds !== undefined) {
      for (const estadoId of estadosIds) {
        buscarUrl = buscarUrl + '&estadoIds=' + estadoId;
      }
    }

    if (sedesIds !== undefined) {
      for (const sedeId of sedesIds) {
        buscarUrl = buscarUrl + '&sedeIds=' + sedeId;
      }
    }

    if (fechaDesde !== undefined && fechaDesde !== null) {
      buscarUrl = buscarUrl + '&fechaDesde=' + fechaDesde;
    }

    if (fechaHasta !== undefined && fechaHasta !== null) {
      buscarUrl = buscarUrl + '&fechaHasta=' + fechaHasta;
    }

    if (inscritoID !== undefined && inscritoID != null && inscritoID.trim() !== '') {
      buscarUrl = buscarUrl + '&inscritoID=' + inscritoID.trim();
    }

    if (asistenteID !== undefined && asistenteID != null && asistenteID.trim() !== '') {
      buscarUrl = buscarUrl + '&asistenteID=' + asistenteID.trim();
    }

    return this.http.get<Evento[]>(buscarUrl)
      .pipe(
        catchError(this.handleError('searchEventos', []))
      );
  }

  createEvents(evento: Evento): Observable<Evento> {
    return this.http.post<Evento>(`${this.apiUrl}`, evento, httpOptions)
      .pipe(
        catchError(this.handleError('createEvents', evento))
      );
  }

  deleteEvents(id: number): Observable<{}> {
    return this.http.delete(`${this.apiUrl}/${id}`, httpOptions)
      .pipe(
        catchError(this.handleError('deleteEvents'))
      );
  }

  updateEvents(evento: Evento): Observable<Evento> {
    return this.http.put<Evento>(`${this.apiUrl}/${evento.id}`, evento, httpOptions)
      .pipe(
        catchError(this.handleError('updateEvents', evento))
      );
  }
}
