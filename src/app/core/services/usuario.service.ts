import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpErrorHandler, HandleError } from './others/http-error-handler.service';
import { Router } from '@angular/router';
import { Usuario } from '../models/usuario.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private handleError: HandleError;
  apiUrl: string;

  constructor(
    private http: HttpClient,
    private router: Router,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('UsuarioService');
    this.apiUrl = environment.apiUrl + 'UsuarioEventos';
  }

  getUsuario(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(`${this.apiUrl}`)
      .pipe(
        catchError(this.handleError('getUsuario', []))
      );
  }

  getUsuarioId(id: string): Observable<any[] | Usuario> {
    return this.http.get<Usuario>(`${this.apiUrl}/${id}`)
      .pipe(
        catchError(this.handleError('getUsuarioId', []))
      );
  }

  searchUsuarios(email: string, userId: string): Observable<Usuario[]> {
    let buscarUrl = `${this.apiUrl}/buscar?`;

    if (email !== undefined && email !== null && email.trim() !== '') {
      buscarUrl = buscarUrl + '&email=' + email.trim();
    }

    if (userId !== undefined && userId !== null && userId.trim() !== '') {
      buscarUrl = buscarUrl + '&userId=' + userId.trim();
    }

    return this.http.get<Usuario[]>(buscarUrl)
      .pipe(
        catchError(this.handleError('searchUsuarios', []))
      );
  }

  createUsuario(usuario: Usuario): Observable<Usuario> {
    const body = JSON.stringify(usuario);
    return this.http.post<Usuario>(`${this.apiUrl}`, body, httpOptions)
      .pipe(
        catchError(this.handleError('createUsuario', usuario))
      );
  }

  deleteUsuario(id: string): Observable<{}> {
    return this.http.delete(`${this.apiUrl}/${id}`, httpOptions)
      .pipe(
        catchError(this.handleError('deleteUsuario'))
      );
  }

  updateUsuario(usuario: Usuario): Observable<Usuario> {
    return this.http.put<Usuario>(`${this.apiUrl}/${usuario.usuarioID}`, usuario, httpOptions)
      .pipe(
        catchError(this.handleError('updateUsuario', usuario))
      );
  }
}
