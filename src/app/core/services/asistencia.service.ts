import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpErrorHandler, HandleError } from './others/http-error-handler.service';
import { Router } from '@angular/router';
import { Asistencia } from '../models/asistencia.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class AsistenciaService {
  private handleError: HandleError;
  apiUrl: string;

  constructor(
    private http: HttpClient,
    private router: Router,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('AsistenciaService');
    this.apiUrl = environment.apiUrl + 'Asistencias';
  }

  getAsistencia(): Observable<Asistencia[]> {
    return this.http.get<Asistencia[]>(`${this.apiUrl}`)
      .pipe(
        catchError(this.handleError('getAsistencia', []))
      );
  }

  getAsistenciaId(id: string): Observable<any[] | Asistencia> {
    return this.http.get<Asistencia>(`${this.apiUrl}/${id}`)
      .pipe(
        catchError(this.handleError('getAsistenciaId', []))
      );
  }

  searchAsistencias(eventoId: number, userId: string): Observable<Asistencia[]> {
    let buscarUrl = `${this.apiUrl}/buscar?`;

    if (eventoId !== undefined && eventoId !== null) {
      buscarUrl = buscarUrl + '&eventoID=' + eventoId;
    }

    if (userId !== undefined && userId !== null && userId.trim() !== '') {
      buscarUrl = buscarUrl + '&userId=' + userId.trim();
    }

    return this.http.get<Asistencia[]>(buscarUrl)
      .pipe(
        catchError(this.handleError('searchAsistencias', []))
      );
  }

  createAsistencia(asistencia: Asistencia): Observable<Asistencia> {
    const body = JSON.stringify(asistencia);
    return this.http.post<Asistencia>(`${this.apiUrl}`, body, httpOptions)
      .pipe(
        catchError(this.handleError('createAsistencia', asistencia))
      );
  }

  deleteAsistencia(id: string): Observable<{}> {
    return this.http.delete(`${this.apiUrl}/${id}`, httpOptions)
      .pipe(
        catchError(this.handleError('deleteAsistencia'))
      );
  }

  updateAsistencia(asistencia: Asistencia): Observable<Asistencia> {
    return this.http.put<Asistencia>(`${this.apiUrl}/${asistencia.id}`, asistencia, httpOptions)
      .pipe(
        catchError(this.handleError('updateAsistencia', asistencia))
      );
  }
}
