import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpErrorHandler, HandleError } from './others/http-error-handler.service';
import { Router } from '@angular/router';
import { Item } from '../models/item.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class ItemService {
  private handleError: HandleError;
  apiUrl: string;

  constructor(
    private http: HttpClient,
    private router: Router,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('ItemService');
    this.apiUrl = environment.apiUrl + 'Items';
  }

  getItem(): Observable<Item[]> {
    return this.http.get<Item[]>(`${this.apiUrl}`)
      .pipe(
        catchError(this.handleError('getItem', []))
      );
  }

  getItemId(id: string): Observable<any[] | Item> {
    return this.http.get<Item>(`${this.apiUrl}/${id}`)
      .pipe(
        catchError(this.handleError('getItemId', []))
      );
  }

  getItemPorDominio(id: string): Observable<any[] | Item> {
    return this.http.get<Item>(`${this.apiUrl}/buscarPorDominio/${id}`)
      .pipe(
        catchError(this.handleError('getItemPorDominio', []))
      );
  }

  createItem(item: Item): Observable<Item> {
    const body = JSON.stringify(item);
    return this.http.post<Item>(`${this.apiUrl}`, body, httpOptions)
      .pipe(
        catchError(this.handleError('createItem', item))
      );
  }

  deleteItem(id: string): Observable<{}> {
    return this.http.delete(`${this.apiUrl}/${id}`, httpOptions)
      .pipe(
        catchError(this.handleError('deleteItem'))
      );
  }

  updateItem(item: Item): Observable<Item> {
    return this.http.put<Item>(`${this.apiUrl}/${item.id}`, item, httpOptions)
      .pipe(
        catchError(this.handleError('updateItem', item))
      );
  }
}
