import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpErrorHandler, HandleError } from './others/http-error-handler.service';
import { Router } from '@angular/router';
import { Inscripcion } from '../models/inscripcion.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class InscripcionService {
  private handleError: HandleError;
  apiUrl: string;

  constructor(
    private http: HttpClient,
    private router: Router,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('InscripcionService');
    this.apiUrl = environment.apiUrl + 'Inscripciones';
  }

  getInscripcion(): Observable<Inscripcion[]> {
    return this.http.get<Inscripcion[]>(`${this.apiUrl}`)
      .pipe(
        catchError(this.handleError('getInscripcion', []))
      );
  }

  getInscripcionId(id: string): Observable<any[] | Inscripcion> {
    return this.http.get<Inscripcion>(`${this.apiUrl}/${id}`)
      .pipe(
        catchError(this.handleError('getInscripcionId', []))
      );
  }

  searchInscripciones(eventoId: number, userId: string): Observable<Inscripcion[]> {
    var buscarUrl = `${this.apiUrl}/buscar?`;

    if (eventoId !== undefined && eventoId !== null) {
      buscarUrl = buscarUrl + '&eventoID=' + eventoId;
    }

    if (userId !== undefined && userId !== null && userId.trim() !== '') {
      buscarUrl = buscarUrl + '&userId=' + userId.trim();
    }

    return this.http.get<Inscripcion[]>(buscarUrl)
      .pipe(
        catchError(this.handleError('searchInscripciones', []))
      );
  }

  createInscripcion(inscripcion: Inscripcion): Observable<Inscripcion> {
    const body = JSON.stringify(inscripcion);
    return this.http.post<Inscripcion>(`${this.apiUrl}`, body, httpOptions)
      .pipe(
        catchError(this.handleError('createInscripcion', inscripcion))
      );
  }

  deleteInscripcion(id: string): Observable<{}> {
    return this.http.delete(`${this.apiUrl}/${id}`, httpOptions)
      .pipe(
        catchError(this.handleError('deleteInscripcion'))
      );
  }

  updateInscripcion(inscripcion: Inscripcion): Observable<Inscripcion> {
    return this.http.put<Inscripcion>(`${this.apiUrl}/${inscripcion.id}`, inscripcion, httpOptions)
      .pipe(
        catchError(this.handleError('updateinscripcion', inscripcion))
      );
  }
}
