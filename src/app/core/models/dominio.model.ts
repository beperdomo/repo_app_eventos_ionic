import { Item } from './item.model';

export class Dominio {
    id: number;
    nombre: string;
    descripcion: string;
    items: Item[];
}
