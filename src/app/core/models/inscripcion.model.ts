import { Evento } from './evento.model';
import { Usuario } from './usuario.model';

export class Inscripcion {
  id: number;
  eventoID: number;
  evento: Evento;
  usuarioID: string;
  usuario: Usuario;
  fechaRegistro: Date;

  constructor(
    eventoID: number,
    usuarioID: string
  ) {
    this.eventoID = eventoID;
    this.usuarioID = usuarioID;
  }
}
