import { Dominio } from './dominio.model';

export class Item  {
    id: number;
    nombre: string;
    descripcion: string;
    dominioID: number;
    dominio: Dominio;
}