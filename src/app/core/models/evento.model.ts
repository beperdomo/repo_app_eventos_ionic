import { Usuario } from './usuario.model';
import { Item } from './item.model';
import { Asistencia } from './asistencia.model';
import { Inscripcion } from './inscripcion.model';

export class Evento {
  id: number;
  nombre: string;
  descripcion: string;
  responsableID: string;
  responsable: Usuario;
  estadoID: number;
  estado: Item;
  sedeID: number;
  sede: Item;
  requiereInscripcionPrevia: boolean;
  asistencias: Asistencia[];
  inscripciones: Inscripcion[];
  fecha: Date;
  clave: string;

  constructor(
    nombre: string,
    descripcion: string,
    responsableID: string,
    sedeID: number,
    requiereInscripcionPrevia: boolean,
    fecha: Date,
    clave: string
  ) {
    this.nombre = nombre;
    this.descripcion = descripcion;
    this.responsableID = responsableID;
    this.sedeID = sedeID;
    this.requiereInscripcionPrevia = requiereInscripcionPrevia;
    this.fecha = fecha;
    this.clave = clave;
  }
}
