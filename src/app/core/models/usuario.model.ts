import { Item } from './item.model';

export class Usuario {
  usuarioID: string;
  tipoID: number;
  tipo: Item;
  nombre: string;
  apellido: string;
  email: string;

  constructor(
    usuarioID: string,
    tipoID: number,
    nombre: string,
    apellido: string,
    email: string
  ) {
    this.usuarioID = usuarioID;
    this.tipoID = tipoID;
    this.nombre = nombre;
    this.apellido = apellido;
    this.email = email;
  }
}
