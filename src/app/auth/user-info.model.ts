// User info is completely independent of what you have set up for your server
export interface IUserInfo {
  display_name: string;
  role: string;
  email: string;
  user_name: string;
  auth_time?: Number;
  aud?: string;
  email_verified?: Boolean;
  exp?: Number;
  family_name?: string;
  given_name?: string;
  iat?: Number;
  iss?: string;
  jti?: string;
  locale?: string;
  name?: string;
  nonce?: string;
  preferred_username?: string;
  sub: string;
  updated_at?: Number;
  ver?: Number;
  zoneinfo?: string;
  [propName: string]: any;
}
