import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { IAuthAction, AuthActions } from 'ionic-appauth';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {
  action: IAuthAction;
  authenticated: boolean;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.authService.authObservable.subscribe((action) => {
      this.action = action;
      if (action.action === AuthActions.SignInSuccess || action.action === AuthActions.AutoSignInSuccess) {
        this.authenticated = true;
      } else if (action.action === AuthActions.SignOutSuccess) {
        this.authenticated = false;
      }
    });
    if (!this.authenticated) {
      this.authService.signIn().catch(error => console.error(`Sign in error: ${error}`));
    }
  }
}
