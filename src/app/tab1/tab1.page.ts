import { Component, OnInit } from '@angular/core';
import { EventoService } from '../core/services/evento.service';
import { Evento } from '../core/models/evento.model';
import { IAuthAction, AuthActions } from 'ionic-appauth';
import { AuthService } from '../auth/auth.service';
import { IUserInfo } from '../auth/user-info.model';
import { UsuarioService } from '../core/services/usuario.service';
import { Usuario } from '../core/models/usuario.model';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  eventos: Evento[];
  action: IAuthAction;
  authenticated: boolean;
  userDetails: IUserInfo;

  constructor(
    private eventoService: EventoService,
    private authService: AuthService,
    private usuarioService: UsuarioService,
  ) { }

  async ngOnInit() {
    this.authService.authObservable.subscribe((action) => {
      this.action = action;
      if (action.action === AuthActions.SignInSuccess || action.action === AuthActions.AutoSignInSuccess) {
        this.authenticated = true;
      } else if (action.action === AuthActions.SignOutSuccess) {
        this.authenticated = false;
      }
    });
    if (this.authenticated) {
      this.userDetails = await this.authService.getUserInfo<IUserInfo>();
      this.usuarioService.searchUsuarios('', this.userDetails.preferred_username).subscribe(
        (usuarios: Usuario[]) => {
          if (usuarios.length === 0) {
            const nuevoUsuario = new Usuario(
              this.userDetails.preferred_username, 7, this.userDetails.firstName, this.userDetails.lastName, this.userDetails.email);
            this.usuarioService.createUsuario(nuevoUsuario)
              .subscribe(res => {
                console.log(res);
              });
          }
        });
      try {
        this.ListarEventos(undefined, undefined, undefined, undefined);
      } catch (e) {
        console.log('Error al cargar eventos', e);
      }
    } else {
      this.authService.signIn().catch(error => console.error(`Sign in error: ${error}`));
    }
  }

  async ListarEventos(nombre: string, sedesIds: number[], fechaDesde: Date, fechaHasta: Date) {
    this.eventoService
      .searchEventos(undefined, undefined, [2, 3, 4], undefined, undefined, undefined, undefined, undefined)
      .subscribe(
        (eventos: Evento[]) => this.eventos = eventos);
  }

  async Actualizar() {
    this.ListarEventos(undefined, undefined, undefined, undefined);
  }

  signOut() {
    this.authService.signOut();
  }
}
