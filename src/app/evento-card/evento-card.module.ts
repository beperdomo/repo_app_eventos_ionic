import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventoCardComponent } from './evento-card.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    RouterModule
  ],
  declarations: [EventoCardComponent],
  exports: [EventoCardComponent]
})
export class EventoCardModule {}
