import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Evento } from 'src/app/core/models/evento.model';

@Component({
  selector: 'app-evento-card',
  templateUrl: './evento-card.component.html',
  styleUrls: ['./evento-card.component.scss'],
})
export class EventoCardComponent implements OnInit {

  @Input() evento: Evento;
  @Input() mostrarMasInformacion: boolean;
  @Output() recargar = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }
}
