import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { IUserInfo } from '../auth/user-info.model';
import { AuthActions, IAuthAction } from 'ionic-appauth';
import { Evento } from '../core/models/evento.model';
import { EventoService } from '../core/services/evento.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {
  eventos: Evento[];
  action: IAuthAction;
  authenticated: boolean;
  userDetails: IUserInfo;

  constructor(
    private eventoService: EventoService,
    private authService: AuthService
  ) { }

  async ngOnInit() {
    this.authService.authObservable.subscribe((action) => {
      this.action = action;
      if (action.action === AuthActions.SignInSuccess || action.action === AuthActions.AutoSignInSuccess) {
        this.authenticated = true;
      } else if (action.action === AuthActions.SignOutSuccess) {
        this.authenticated = false;
      }
    });
    if (this.authenticated) {
      this.userDetails = await this.authService.getUserInfo<IUserInfo>();
    } else {
      this.authService.signIn().catch(error => console.error(`Sign in error: ${error}`));
    }

    try {
      this.ListarEventos(undefined, undefined, undefined, undefined);
    } catch (e) {
      console.log('Error al cargar eventos', e);
    }
  }

  async ListarEventos(nombre: string, sedesIds: number[], fechaDesde: Date, fechaHasta: Date) {
    this.eventoService
    .searchEventos(undefined, undefined, [2, 3, 4], undefined, undefined, undefined, 
      this.userDetails.preferred_username, this.userDetails.preferred_username)
      .subscribe(
        (eventos: Evento[]) => this.eventos = eventos);
  }

  async Actualizar() {
    this.ListarEventos(undefined, undefined, undefined, undefined);
  }

  signOut() {
    this.authService.signOut();
  }
}
