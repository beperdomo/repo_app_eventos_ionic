import { Component, OnInit } from '@angular/core';
import { Evento } from '../core/models/evento.model';
import { EventoService } from '../core/services/evento.service';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { InscripcionService } from '../core/services/inscripcion.service';
import { Inscripcion } from '../core/models/inscripcion.model';
import { AsistenciaService } from '../core/services/asistencia.service';
import { Asistencia } from '../core/models/asistencia.model';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  eventoId: string;
  eventoClave: string;
  eventoMensaje: string;
  datosEvento: string;
  evento: Evento;
  buscar: boolean;
  registrar: boolean;
  mensajeQr: string;

  constructor(
    private eventoService: EventoService,
    private inscripcionService: InscripcionService,
    private asistenciaService: AsistenciaService,
    private qrScanner: QRScanner
  ) {
    this.buscar = true;
    this.registrar = false;
  }

  async ngOnInit() {
    this.buscar = true;
    this.registrar = false;
  }

  private consultarEvento() {
    this.eventoMensaje = '';
    if (! this.eventoId) {
this.eventoMensaje = 'Ingrese el ID del evento';
    } else if (!this.eventoClave) {
      this.eventoMensaje = 'Ingrese la clave del evento';
    } else {
    this.eventoService
      .getEventsId(this.eventoId)
      .subscribe(
        (evento: Evento) => {
          this.evento = evento;
          if (this.evento === undefined) {
            this.eventoMensaje = 'Evento no existe';
          } else if (this.evento.clave === this.eventoClave) {
            this.buscar = false;
            this.registrar = true;
            this.datosEvento = this.evento.nombre;
          } else {
            this.eventoMensaje = 'La clave no corresponde';
          }
        }
      );
    }
  }

  nuevoEvento() {
    this.eventoId = '';
    this.eventoClave = '';
    this.eventoMensaje = '';
    this.buscar = true;
    this.registrar = false;
  }

  leerQR() {
    this.mensajeQr = '';
    // Optionally request the permission early
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          // camera permission was granted
          // start scanning
          const scanSub = this.qrScanner.scan().subscribe((text: string) => {
            this.inscripcionService
              .getInscripcionId(text)
              .subscribe((inscripcion: Inscripcion) => {
                this.asistenciaService
                  .searchAsistencias(inscripcion.eventoID, inscripcion.usuarioID)
                  .subscribe((asistencias: Asistencia[]) => {
                    if (asistencias.length > 0) {
                      this.mensajeQr = 'Asistencia previamente registrada';
                    } else {
                      if (this.evento.id === inscripcion.eventoID) {
                        const nuevaAsistencia = new Asistencia(inscripcion.eventoID, inscripcion.usuarioID);
                        this.asistenciaService.createAsistencia(nuevaAsistencia)
                          .subscribe(res => {
                            this.mensajeQr = 'Asistencia registrada';
                          });
                      } else {
                        this.mensajeQr = 'Inscripción no corresponde a evento que se está registrando';
                      }
                    }
                  });
              });
            this.qrScanner.hide(); // hide camera preview
            scanSub.unsubscribe(); // stop scanning
          });

        } else if (status.denied) {
          // camera permission was permanently denied
          // you must use QRScanner.openSettings() method to guide the user to the settings page
          // then they can grant the permission from there
        } else {
          // permission was denied, but not permanently. You can ask for permission again at a later time.
        }
      })
      .catch((e: any) => this.mensajeQr = 'Error:' + e);
  }
}
