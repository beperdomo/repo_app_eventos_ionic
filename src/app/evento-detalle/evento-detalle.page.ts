import { Component, OnInit } from '@angular/core';
import { Evento } from '../core/models/evento.model';
import { Inscripcion } from '../core/models/inscripcion.model';
import { Asistencia } from '../core/models/asistencia.model';
import { IUserInfo } from '../auth/user-info.model';
import { IAuthAction, AuthActions } from 'ionic-appauth';
import { EventoService } from '../core/services/evento.service';
import { InscripcionService } from '../core/services/inscripcion.service';
import { AsistenciaService } from '../core/services/asistencia.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-evento-detalle',
  templateUrl: './evento-detalle.page.html',
  styleUrls: ['./evento-detalle.page.scss'],
})
export class EventoDetallePage implements OnInit {

  evento: Evento;
  inscripcion: Inscripcion;
  asistencia: Asistencia;
  loading = true;
  isAuthenticated: boolean;
  private userDetails: IUserInfo;
  action: IAuthAction;
  sub: any;

  mensajeInscripcion: string;
  mostrarCardInscripcion: boolean;
  mostrarCodigoQR: boolean;
  codigoQR: string;
  mostrarBotonInscribirse: boolean;

  mensajeAsistencia: string;
  mostrarBotonAsistir: boolean;


  constructor(
    private eventoService: EventoService,
    private inscripcionService: InscripcionService,
    private asistenciaService: AsistenciaService,
    public authService: AuthService,
    private route: ActivatedRoute,
  ) {
  }

  async ngOnInit() {
    this.authService.authObservable.subscribe((action) => {
      this.action = action;
      if (action.action === AuthActions.SignInSuccess || action.action === AuthActions.AutoSignInSuccess) {
        this.isAuthenticated = true;
      } else if (action.action === AuthActions.SignOutSuccess) {
        this.isAuthenticated = false;
      }
    });

    if (this.isAuthenticated) {
      this.userDetails = await this.authService.getUserInfo<IUserInfo>();
    }
    this.GetEventDetail();
  }

  GetEventDetail() {
    this.mostrarCardInscripcion = false;
    this.mostrarBotonInscribirse = false;
    this.mostrarCodigoQR = false;
    this.codigoQR = '';
    this.mensajeInscripcion = '';

    this.mostrarBotonAsistir = false;
    this.mensajeAsistencia = '';


    this.sub = this.route.params.subscribe(params => {
      this.eventoService
        .getEventsId(params.id)
        .subscribe(
          (evento: Evento) => {
            this.evento = evento;
            this.mostrarCardInscripcion = this.evento.requiereInscripcionPrevia;

            if (this.evento.requiereInscripcionPrevia) {
              this.consultarInscripcion();
            }

            this.consultarAsistencia();
            this.loading = false;
          }
        );
    });
  }

  private consultarInscripcion() {
    this.inscripcionService
      .searchInscripciones(this.evento.id, this.userDetails.preferred_username)
      .subscribe((inscripciones: Inscripcion[]) => {
        if (inscripciones.length > 0) {
          this.inscripcion = inscripciones[0];
        }

        switch (this.evento.estadoID) {
          case 2: // Publicado
            if (this.inscripcion === undefined) {
              this.mensajeInscripcion = 'La inscripción a este evento está abierta. Por favor inscríbase';
              this.mostrarBotonInscribirse = true;
            } else {
              this.visualizarInscripcion();
            }

            break;

          case 3: // En curso
            if (this.inscripcion === undefined) {
              this.mensajeInscripcion = 'La inscripción a este evento está cerrada.';
            } else {
              this.visualizarInscripcion();
            }
            break;

          case 4: // Realizado
            if (this.inscripcion === undefined) {
              this.mensajeInscripcion = 'La inscripción a este evento está cerrada.';
            } else {
              this.visualizarInscripcion();
            }
            break;
        }
      });
  }

  visualizarInscripcion() {
    this.mensajeInscripcion = 'Gracias por inscribirse. Por favor presente el siguiente código al ingresar al evento';
    this.mostrarCodigoQR = true;
    this.mostrarBotonInscribirse = false;
    this.codigoQR = this.inscripcion.id.toString();
  }


  agregarInscripcion() {
    const nuevaInscripcion = new Inscripcion(this.evento.id, this.userDetails.preferred_username);
    this.inscripcionService.createInscripcion(nuevaInscripcion)
      .subscribe(res => {
        this.consultarInscripcion();
      });
  }

  private consultarAsistencia() {
    this.asistenciaService
      .searchAsistencias(this.evento.id, this.userDetails.preferred_username)
      .subscribe((asistencias: Asistencia[]) => {
        if (asistencias.length > 0) {
          this.asistencia = asistencias[0];
        }
        switch (this.evento.estadoID) {
          case 2: // Publicado
            this.mensajeAsistencia = 'El registro de asistencia a este evento no está aún disponible';
            break;

          case 3: // En curso
            if (this.evento.requiereInscripcionPrevia && (this.inscripcion === undefined)) {
              this.mensajeAsistencia = 'Por favor muestre el código a la entrada del evento para registrar su asistencia.';
            } else if (this.asistencia === undefined) {
              this.mostrarBotonAsistir = true;
              this.mensajeAsistencia = 'El registro de asistencia a este evento está disponible. Por favor registre su asistencia.';
            } else {
              this.visualizarAsistencia();
            }

            break;

          case 4: // Realizado
            if (this.asistencia === undefined) {
              this.mensajeAsistencia = 'El registro de asistencia a este evento está cerrado.';
            } else {
              this.visualizarAsistencia();
            }
            break;
        }
      });
  }

  visualizarAsistencia() {
    this.mensajeAsistencia = 'Gracias por registrar su asistencia.';
    this.mostrarBotonAsistir = false;
  }

  agregarAsistencia() {
    const nuevaAsistencia = new Asistencia(this.evento.id, this.userDetails.preferred_username);
    this.asistenciaService.createAsistencia(nuevaAsistencia)
      .subscribe(res => {
        this.consultarAsistencia();
      });
  }
}
