import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { QRCodeModule } from 'angularx-qrcode';

import { EventoDetallePageRoutingModule } from './evento-detalle-routing.module';
import { EventoDetallePage } from './evento-detalle.page';
import { EventoCardModule } from '../evento-card/evento-card.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EventoCardModule,
    EventoDetallePageRoutingModule,
    QRCodeModule
  ],
  declarations: [EventoDetallePage]
})
export class EventoDetallePageModule {}
